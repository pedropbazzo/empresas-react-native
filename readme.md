# Índice

- [Sobre](#sobre)
- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Como Usar](#como-usar)


## Sobre

É uma aplicação Mobile desenvolvida como parte do desafio de React Native para a empresa Ioasys.
Um aplicativo que tem as funcionalidade de Login, Listagem das Empresas, Busca por empresa e Profile do usuário logado.

## :rocket: Tecnologias Utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias

- [React Native](https://reactnative.dev/) <br>
  Dependências utilizadas e explicações sobre elas:

  * @react-native-async-storage/async-storage:

    * `AsyncStorage é uma API nativa do React Native, utilizada para armazenar dados persistentes no dispositivo. É uma forma de salvar dados no formato chave e valor`;

  * @react-navigation:

    * `É uma biblioteca popular para roteamento e navegação em uma aplicação React Native. Essa biblioteca ajuda a resolver o problema de navegar entre várias telas e compartilhar dados entre elas. Aqui foram utilizados a <strong>@react-navigation/bottom-tabs</strong> e a <strong>@react-navigation/stack</strong>`;

  * axios:

    * `Axios é um cliente HTTP, que funciona tanto no browser quanto em node.js. A biblioteca é basicamente uma API. Serve para estruturar e configurar as chamadas da api`;

  * react-native-gesture-handler:

    * `Uma biblioteca que nos auxilia a lidar com gestos do usuário`;

  * react-native-vector-icons

    * `Biblioteca que utilizada para adicionar ícones no projeto. Aqui foi utilizado os ícones do Feather`;

  * styled-components:

    * `É uma biblioteca que utiliza o conceito de CSS-in-JS, ou seja, que nos permite escrever códigos CSS dentro do Javascript. Nos da praticidade. Criando códigos dessa forma, podemos reaproveitar o CSS de um projeto de forma mais rápida e ainda escrevê-lo sem revezar entre estilos diferentes de escrita`.


## :fire: Como usar

- ### **Pré-requisitos**

  - É **necessário** possuir o **[Node.js](https://nodejs.org/en/)** instalado na máquina
  - Também, é **preciso** ter um gerenciador de pacotes seja o **[NPM](https://www.npmjs.com/)** ou **[Yarn](https://yarnpkg.com/)**.

1. Executando a Aplicação:

```sh

  # Clone o repositório

  # Instale as dependências
  $ npm install ou yarn

  # Inicie a aplicação mobile
  $ cd nome da pasta
  $ npm start ou yarn android
```

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.
